package binbin

import (
	"bytes"
	"fmt"
	"regexp"
	"strings"
	"text/template"
)

const (
	BaseCommand = "docker run --interactive --tty"
	pwd         = "`pwd`"
)

func cleanSpaces(in string) string {
	// return strings.Replace(in, "  ", " ", -1)
	return regexp.MustCompile(` {2,}`).ReplaceAllString(in, " ")
}

type ConfigSet []*Config

func (cs ConfigSet) Render() (string, error) {
	var output string
	for _, c := range cs {
		s, err := c.Render()
		if err != nil {
			return "", err
		}
		output += s
	}
	return output, nil
}

type Config struct {
	CmdName          string            `json:"cmd_name"`
	CmdArgs          map[string]string `json:"cmd_args"`
	CmdFlags         string            `json:"cmd_flags"`
	ArgReceiver      string            `json:"arg_receiver"`
	ContainerName    string            `json:"container_name"`
	NeedsPWD         bool              `json:"needs_pwd"`
	NeedsHostNetwork bool              `json:"needs_host_net"`
	CmdEnvVars       map[string]string `json:"cmd_env_vars"`
	EnvVars          map[string]string `json:"env_vars"`
	Volumes          map[string]string `json:"volumes"` // host:container
	LinuxOnly        bool              `json:"linux_only"`
}

func NewConfig(name string) *Config {
	return &Config{
		CmdName:       name,
		ContainerName: name,
		ArgReceiver:   "$@",
		CmdArgs:       make(map[string]string),
		CmdEnvVars:    make(map[string]string),
		EnvVars:       make(map[string]string),
		Volumes:       make(map[string]string),
	}
}

func (c *Config) WithName(name string) *Config {
	c.CmdName = name
	return c
}

func (c *Config) WithVolume(host, container string) *Config {
	c.Volumes[host] = container
	return c
}

func (c *Config) WithEnvVar(name, value string) *Config {
	c.EnvVars[name] = value
	return c
}

func (c *Config) WithCmdEnvVar(name, value string) *Config {
	c.CmdEnvVars[name] = value
	return c
}

func (c *Config) OnlyOnLinux() *Config {
	c.LinuxOnly = true
	return c
}

func (c *Config) WithNetwork() *Config {
	c.NeedsHostNetwork = true
	return c
}

func (c *Config) Receiving(recv string) *Config {
	c.ArgReceiver = recv
	return c
}

func (c *Config) WithContainerName(container string) *Config {
	c.ContainerName = container
	return c
}

func (c *Config) WithCmdFlag(flag string) *Config {
	c.CmdFlags = flag
	return c
}

func (c *Config) WithPWD() *Config {
	c.NeedsPWD = true
	c.Volumes[pwd] = pwd
	return c
}

func (c *Config) WithCommandArg(name, value string) *Config {
	c.CmdArgs[name] = value
	return c
}

func buildVolumeParams(input map[string]string) string {
	var output string
	for host, container := range input {
		output = fmt.Sprintf("%s %s",
			output,
			buildParams(
				"volume",
				[]string{fmt.Sprintf("%s:%s", host, container)},
			),
		)
	}
	return cleanSpaces(output)
}

func buildParams(argName string, params []string) string {
	var output string
	for _, p := range params {
		output = fmt.Sprintf("%s --%s=%s", output, argName, p)
	}
	return cleanSpaces(output)
}

type function struct {
	CmdName     string
	CmdFlags    string
	NetworkArgs string
	VolumeArgs  string
	Container   string
	EnvVars     string
	CmdEnvVars  string
	CmdArgs     string
}

func joinArgs(argFlag string, input map[string]string) string {
	var out string
	for name, value := range input {
		out += fmt.Sprintf(" %s %s=%s", argFlag, strings.ToUpper(name), value)
	}
	return out
}

func (c *Config) Render() (string, error) {
	if c.CmdName == "" {
		c.CmdName = c.ContainerName
	}

	cmdTmpl := `function {{ .CmdName }}() {{ "{" }}
	docker run --interactive --tty {{ if .NeedsHostNetwork }}--network=host{{ else }}--network=none{{ end }} {{ buildVolumeParams .Volumes }} {{ argjoin "--env" .CmdEnvVars }} verygoodsoftwarenotvirus/{{- .ContainerName }} {{ argjoin "" .CmdArgs }} {{ .CmdFlags }} {{ .ArgReceiver }}
{{ "}" }}

`

	var tpl bytes.Buffer
	t, err := template.New("t").
		Funcs(map[string]interface{}{
			"argjoin":           joinArgs,
			"buildVolumeParams": buildVolumeParams,
			"buildParams":       buildParams,
		}).
		Parse(cmdTmpl)

	if err != nil {
		return "", nil
	}

	t.Execute(&tpl, c)
	return cleanSpaces(tpl.String()), nil
}
