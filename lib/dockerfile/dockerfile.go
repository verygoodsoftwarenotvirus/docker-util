package dockerfile

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"
)

func cleanSpaces(in string) string {
	return regexp.MustCompile(`\s{2,}`).ReplaceAllString(in, " ")
}

type instructionType string

const (
	ENTRYPOINT instructionType = "ENTRYPOINT"
	ADD        instructionType = "ADD"
	COPY       instructionType = "COPY"
	RUN        instructionType = "RUN"
	WORKDIR    instructionType = "WORKDIR"
	CMD        instructionType = "CMD"
)

type Instruction interface {
	Command() string
}

var _ Instruction = (*CommonInstruction)(nil)

type CommonInstruction struct {
	Type instructionType
	Args []string
}

func (i *CommonInstruction) Command() string {
	return fmt.Sprintf(`%s [ "%s" ]`, i.Type, strings.Join(i.Args, `", "`))
}

type Stage struct {
	BaseImage    string
	BaseImageTag string
	Name         string
	Instructions []Instruction
}

func NewStage() *Stage {
	return &Stage{
		BaseImageTag: "latest",
		Instructions: []Instruction{},
	}
}

func (s *Stage) FROM(baseImage string) *Stage {
	s.BaseImage = baseImage
	return s
}

var _ Instruction = (*FromInstruction)(nil)

type FromInstruction struct {
	BaseImage string
	Tag       string
	StageName string
}

func (fi FromInstruction) Command() string {
	if fi.StageName != "" {
		return fmt.Sprintf("FROM %s:%s AS %s", fi.BaseImage, fi.Tag, fi.StageName)
	}
	return fmt.Sprintf("FROM %s:%s", fi.BaseImage, fi.Tag)
}

func (s *Stage) Tag(tag string) *Stage {
	s.BaseImageTag = tag
	return s
}

func (s *Stage) RUN(cmd ...string) *Stage {
	s.Instruct(&CommonInstruction{
		Type: RUN,
		Args: cmd,
	})
	return s
}

var _ Instruction = (*RunInstruction)(nil)

type RunInstruction struct {
	CmdArgs []string
}

func (ri RunInstruction) Command() string {
	return fmt.Sprintf(`RUN [ "%s" ]`, strings.Join(ri.CmdArgs, `", "`))
}

func (s *Stage) COPY(in *CopyInstruction) *Stage {
	return s.Instruct(in)
}

var _ Instruction = (*CopyInstruction)(nil)

type CopyInstruction struct {
	Source      string
	Destination string
	From        string
}

func (ci CopyInstruction) Command() string {
	if ci.From != "" {
		return cleanSpaces(fmt.Sprintf("COPY --from=%s %s %s", ci.From, ci.Source, ci.Destination))
	}
	return cleanSpaces(fmt.Sprintf("COPY %s %s", ci.Source, ci.Destination))
}

func (s *Stage) CopyFrom(in *CopyInstruction) *Stage {
	return s.Instruct(in)
}

func (s *Stage) Instruct(instructions ...Instruction) *Stage {
	s.Instructions = append(s.Instructions, instructions...)
	return s
}

type Dockerfile struct {
	path             string
	RepoURI          string
	CurrentStage     *Stage
	Stages           []*Stage
	Instructions     []Instruction
	FinalInstruction Instruction
}

func New(path string) *Dockerfile {
	ns := NewStage()
	return &Dockerfile{
		path:         path,
		Instructions: []Instruction{},
		CurrentStage: ns,
		Stages:       []*Stage{ns},
	}
}

func (df *Dockerfile) Next() {
	df.CurrentStage = NewStage()
	df.Stages = append(df.Stages, df.CurrentStage)
}

func (df *Dockerfile) ProjectURL(uri string) *Dockerfile {
	df.RepoURI = uri
	return df
}

func (df *Dockerfile) ENTRYPOINT(args ...string) *Dockerfile {
	df.FinalInstruction = &CommonInstruction{
		Type: ENTRYPOINT,
		Args: args,
	}
	return df
}

var _ Instruction = (*EntrypointInstruction)(nil)

type EntrypointInstruction struct {
	CmdArgs []string
}

func (ei EntrypointInstruction) Command() string {
	return fmt.Sprintf(`ENTRYPOINT [ "%s" ]`, strings.Join(ei.CmdArgs, `", "`))
}

func (df *Dockerfile) Cmd(args ...string) *Dockerfile {
	df.FinalInstruction = &CommonInstruction{
		Type: CMD,
		Args: args,
	}
	return df
}

func (df *Dockerfile) FROM(baseImage string) *Dockerfile {
	df.CurrentStage.BaseImage = baseImage
	return df
}

func (df *Dockerfile) Tag(tag string) *Dockerfile {
	df.CurrentStage.BaseImageTag = tag
	return df
}

func (df *Dockerfile) RUN(cmd ...string) *Dockerfile {
	df.CurrentStage.Instruct(&CommonInstruction{
		Type: RUN,
		Args: cmd,
	})
	return df
}

func (df *Dockerfile) COPY(src, dest string) *Dockerfile {
	df.CurrentStage.Instruct(&CommonInstruction{
		Type: COPY,
		Args: []string{
			src,
			dest,
		},
	})
	return df
}

func (df *Dockerfile) Instruct(instructions ...Instruction) *Dockerfile {
	df.CurrentStage.Instruct(instructions...)
	return df
}

func (df *Dockerfile) Render() error {
	fileTmpl := `
{{- if ne .RepoURI "" }}# {{ .RepoURI }}{{ end -}}
{{- range .Stages }}
FROM {{ .BaseImage }}:{{ .BaseImageTag }}{{ if ne .Name "" }} AS {{ .Name }} {{ end }}
{{ range .Instructions }}
	{{ .Command }}
{{- end }}

# RUN [ "adduser", "-S", "-D", "-H", "appuser" ]
# USER appuser

{{- end }}

{{ .FinalInstruction.Type }} [ "{{ join .FinalInstruction.Args "\", \"" }}" ]
`

	t, err := template.New("t").
		Funcs(map[string]interface{}{
			"join": strings.Join,
		}).
		Parse(fileTmpl)

	if err != nil {
		return err
	}

	var out bytes.Buffer
	err = t.Execute(&out, df)
	if err != nil {
		return err
	}

	realPath, err := filepath.Abs(df.path)
	if err != nil {
		return err
	}

	dir := filepath.Dir(realPath)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0777)
		if err != nil {
			return err
		}
	}

	f, createErr := os.Create(realPath)
	if createErr != nil {
		return createErr
	}

	return t.Execute(f, df)
}
